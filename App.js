import React from 'react';
import {
  AppState,
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const App = () => {
  return (
   <View style={styles.container}>
     <View style={styles.content}>
      <View style={styles.header}>
        <Text  style={styles.label}>Digital Approval</Text>
      </View>
      <Image source={require('./gambar.png')} style={styles.image} />
      <TextInput style={styles.input} placeholder='Alamat Email'/>
      <TextInput style={styles.input} placeholder='Password'/>
      <Text style={styles.ResetPassword}>
        Reset Password 
      </Text>
      <Text style={{textAlign: 'right'}}></Text>
      <View style={styles.ButtonLogin}>
        <TouchableOpacity
          style={styles.button} 
        >
          <Text style={styles.ButtonName}>Login</Text>
        </TouchableOpacity>
      </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image:{
    width: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
    
  },

  container: {
    flex: 1,
     justifyContent: 'center',
       alignContent: 'center',
        backgroundColor: 'hsla(0, 0%, 97%, 1)',
          alignContent: 'center'
    
  },
  content: {
    backgroundColor: 'white',
    marginHorizontal: 20
  },

  input: {
    height: 48,
       margin: 12,
         borderWidth: 0.03,
       padding: 16,
     borderRadius: 0.19,
    paddingStart: 20,
  },

  header: {
    alignItems: 'center',
    marginBottom: 10,
    marginTop: -25,
  },

  label: {
    backgroundColor: 'hsla(215, 100%, 17%, 1)',
    fontSize: 20,
    paddingEnd: 30,
    paddingStart: 30,
    paddingTop: 6,
    paddingBottom: 8,
    borderRadius: 21,
    marginBottom: 25,
    color: 'white'
  },

  ButtonLogin: {
    paddingStart: 16,
    paddingEnd: 16,
  },

  ResetPassword: {
    alignSelf: 'flex-end',
    marginTop: 12,
    marginBottom: 24,
    color: 'hsla(214, 78%, 53%, 1)',
    fontStyle: 'italic',
    // backgroundColor: 'aqua',
    paddingRight: 16,

  },

  button: {
    alignItems: "center",
    backgroundColor: "hsla(214, 78%, 53%, 1)",
    marginBottom: 26,
    paddingBottom: 14,
    paddingStart: 120,
    paddingEnd: 120,
    color: 'white',
    borderRadius: 6,
    
  
  },
  ButtonName: {
    color: 'white',
    paddingTop: 13,
    fontWeight : 'bold',
    fontSize: 16,
    
  },
  
  
  
});

export default App;
